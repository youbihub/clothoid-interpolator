#pragma once

#include <Eigen/Dense>
namespace cloth {
  using Lla = Eigen::Array3Xd;
  Eigen::Matrix3Xd Lla2ecef(Lla lla);
}  // namespace cloth
