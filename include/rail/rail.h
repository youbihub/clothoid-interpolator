#pragma once
#include <Eigen/Dense>
#include <vector>

#pragma GCC diagnostic push  // disable a  [-Werror=unused-parameter]. != for msvc, == for clang
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "ceres/ceres.h"
#pragma GCC diagnostic pop

#include "rail/geography.h"

namespace cloth {
  template <typename T> using V3 = Eigen::Matrix<T, 3, 1>;
  template <typename T> using V3n = Eigen::Matrix<T, 3, Eigen::Dynamic>;
  template <typename T> using V1n = Eigen::Matrix<T, 1, Eigen::Dynamic>;

  template <typename Type>  //
  struct Apparatus {
    V3<Type> R;
    V3<Type> T;
    V3<Type> N;
    V3<Type> B;
    Type Curv0;
    Type Tors0;
    bool operator==(const Apparatus<Type>& a) const;
  };

  template <typename Type>
  Apparatus<Type> FrenetTaylor(const Apparatus<Type>& a, const Type& kp, const Type& tp, Type s);

  template <typename Type>
  std::vector<Apparatus<Type>> RailIntegrator(const Apparatus<Type>& app0, const V1n<Type>& C1,
                                              const V1n<Type>& T1, const double& ds);

  struct RailDistance {
    RailDistance(const double& L, const uint& n, const Eigen::Matrix3Xd& measure);
    template <typename T> bool operator()(const T* const x0, const T* const AngleAxis,
                                          const T* const curv0, const T* const tors0,
                                          const T* const curv1, const T* const tors1,
                                          const T* const s, T* residual) const;
    const double L = 200;  // todo: straight to ds? //todo: already globally available?
    const uint n = 10;     //#rail nodes
    const Eigen::Matrix3Xd measure;
  };

  template <typename Type>  // todo: useful? check with the instanciation
  Apparatus<Type> MakeApparatus(const Eigen::Matrix<Type, 3, 1>& xyz,
                                const Eigen::Matrix<Type, 3, 1>& angle_axis, const Type& curv0,
                                const Type& tors0);

  std::tuple<std::vector<Apparatus<double>>, Eigen::VectorXd, Eigen::VectorXd> Lla2rail(
      const Lla& lla);

  // for d>0;
  template <typename T> inline size_t myfloor(T j) { return (size_t)j.a; }
  // template function specialization
  template <> inline size_t myfloor<double>(double d) { return (size_t)d; }
}  // namespace cloth
