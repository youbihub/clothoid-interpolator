#include "rail/rail.h"

#include <iostream>
// #include <nlohmann/json.hpp>

#include "Eigen/Core"
#include "Eigen/Dense"
#include "ceres/ceres.h"
#include "ceres/rotation.h"
#include "tuple"

// TODO: bound
// TODO: 48 as param
// TODO: ceres with dependancy manager

namespace cloth {

  constexpr struct Conf {  // todo: best practice?
    double rail_length = 200;
    uint n_node = 15;
    double max_curv = 0.234;
    double max_tors = 0.123;
    uint n_measure = 10;
  } conf;

  template <typename Type> bool Apparatus<Type>::operator==(const Apparatus<Type>& a) const {
    return (a.R == R) && (a.T == T) && (a.N == N) && (a.B == B) && (a.Curv0 == Curv0)
           && (a.Tors0 == Tors0);
  }

  template <typename Type>
  Apparatus<Type> FrenetTaylor(const Apparatus<Type>& a, const Type& kp, const Type& tp, Type s) {
    auto& R = a.R;
    auto& T = a.T;
    auto& N = a.N;
    auto& B = a.B;
    auto& k = a.Curv0;
    auto& t = a.Tors0;

    auto T1 = k * N;
    auto& R1 = T;
    auto N1 = -k * T + t * B;
    auto B1 = -t * N;

    auto T2 = kp * N + k * N1;
    auto& R2 = T1;
    auto N2 = -kp * T - k * T1 + tp * B + t * B1;
    auto B2 = -tp * N - t * N1;

    auto T3 = kp * N1 + kp * N1 + k * N2;
    auto& R3 = T2;
    auto N3 = -kp * T1 - kp * T1 - k * T2 + tp * B1 + tp * B1 + t * B2;
    auto B3 = -tp * N1 - tp * N1 - t * N2;

    auto coeffs = Eigen::Matrix<Type, 4, 1>{(Type)1, s, s * s / 2., s * s * s / 6.};

    auto DR = Eigen::Matrix<Type, 3, 4>();
    auto DT = Eigen::Matrix<Type, 3, 4>();
    auto DN = Eigen::Matrix<Type, 3, 4>();
    auto DB = Eigen::Matrix<Type, 3, 4>();
    DR << R, R1, R2, R3;
    DT << T, T1, T2, T3;
    DN << N, N1, N2, N3;
    DB << B, B1, B2, B3;

    auto taylor_t = DT * coeffs;
    auto taylor_r = DR * coeffs;
    auto taylor_n = DN * coeffs;
    auto taylor_b = DB * coeffs;

    Apparatus<Type> y;
    y.T = taylor_t;
    y.R = taylor_r;
    y.N = taylor_n;
    y.B = taylor_b;
    y.Curv0 = k + kp * s;
    y.Tors0 = t + tp * s;
    return y;
  }

  template <typename Type>
  std::vector<Apparatus<Type>> RailIntegrator(const Apparatus<Type>& app0, const V1n<Type>& curv1,
                                              const V1n<Type>& tors1, const double& ds) {
    auto n = (size_t)curv1.cols();
    auto apparatus = std::vector<Apparatus<Type>>{n};
    apparatus[0] = app0;

    for (size_t i = 0; i < n - 1; i++) {
      apparatus[i + 1] = FrenetTaylor<Type>(apparatus[i], curv1[i], tors1[i], (Type)ds);
    }
    return apparatus;
  }

  template <typename Type>
  Apparatus<Type> MakeApparatus(const Eigen::Matrix<Type, 3, 1>& xyz,
                                const Eigen::Matrix<Type, 3, 1>& angle_axis, const Type& curv0,
                                const Type& tors0) {
    Apparatus<Type> app;
    app.R = xyz;

    auto rot = Eigen::Matrix<Type, 3, 3>();
    ceres::AngleAxisToRotationMatrix(angle_axis.data(),
                                     ceres::MatrixAdapter<Type, 1, 3>(rot.data()));
    app.T = rot.col(0);
    app.N = rot.col(1);
    app.B = rot.col(2);
    app.Curv0 = curv0;
    app.Tors0 = tors0;
    return app;
  }
  RailDistance::RailDistance(const double& L, const uint& n, const Eigen::Matrix3Xd& measure)
      : L(L), n(n), measure(measure) {}
  template <typename T> bool RailDistance::operator()(const T* const x0, const T* const angle_axis,
                                                      const T* const curv0, const T* const tors0,
                                                      const T* const curv1, const T* const tors1,
                                                      const T* const s, T* residual) const {
    using V3 = Eigen::Matrix<T, 3, 1>;
    const V3 m_x0 = Eigen::Map<const V3>(x0);
    const V3 m_angle_axis = Eigen::Map<const V3>(angle_axis);
    using Vn = Eigen::Matrix<T, 1, Eigen::Dynamic>;
    const Vn m_curv1 = Eigen::Map<const Vn>(curv1, n);
    const Vn m_tors1 = Eigen::Map<const Vn>(tors1, n);
    auto app0 = MakeApparatus(m_x0, m_angle_axis, *curv0, *tors0);
    auto ds = L / n;
    auto rail = RailIntegrator<T>(app0, m_curv1, m_tors1, ds);
    using V3n = Eigen::Matrix<T, 3, Eigen::Dynamic>;
    auto m_residual = Eigen::Map<V3n>(residual, 3, measure.cols());  // todo:correct mapping?
    const V1n<T> m_s = Eigen::Map<const V1n<T>>(s, measure.cols());
    auto n_measure = measure.cols();
    for (long int i = 0; i < n_measure; i++) {
      // auto idx = (size_t)(m_s(i) / ds);  // todo: to specialize (floor function)
      auto idx = myfloor(m_s(i) / ds);
      auto railpoint = FrenetTaylor<T>(rail[idx], m_curv1(i), m_tors1(i), m_s(i) - idx * ds);
      m_residual.col(i) = measure.col(i) - railpoint.R;
    }

    return true;
  }

  std::tuple<std::vector<Apparatus<double>>, Eigen::VectorXd, Eigen::VectorXd> Lla2rail(
      const Lla& lla) {
    // todo: logging (see ceres examples)
    auto xyz = Lla2ecef(lla);
    auto p0 = Eigen::Vector3d{};
    p0 = xyz.col(0);  // todo: one liner? deep copy
    auto aa0 = Eigen::Vector3d();
    auto ux = (xyz(Eigen::all, Eigen::last) - p0).normalized();
    auto uz = p0.normalized();
    auto uy = uz.cross(ux);
    auto R = Eigen::Matrix3d();
    R << ux, uy, uz;
    ceres::RotationMatrixToAngleAxis(R.data(), aa0.data());  // todo: encapsulate in function
    auto curv0 = double{};
    auto tors0 = double{};
    // todo: struct?
    auto ds = conf.rail_length / conf.n_node;
    Eigen::VectorXd s = Eigen::VectorXd::LinSpaced(conf.n_measure, 0, conf.rail_length - ds);
    Eigen::VectorXd curv1 = Eigen::Matrix<double, 1, Eigen::Dynamic>::Zero(conf.n_node);
    Eigen::VectorXd tors1 = Eigen::Matrix<double, 1, Eigen::Dynamic>::Zero(conf.n_node);

    ceres::Problem problem;
    problem.AddResidualBlock(
        new ceres::AutoDiffCostFunction<RailDistance, 3 * conf.n_measure, 3, 3, 1, 1, conf.n_node,
                                        conf.n_node, conf.n_measure>(
            new RailDistance(conf.rail_length, conf.n_node, xyz)),
        NULL, p0.data(), aa0.data(), &curv0, &tors0, curv1.data(), tors1.data(), s.data());
    ceres::Solver::Options options;
    options.max_num_iterations = 25;
    options.linear_solver_type = ceres::DENSE_QR;
    options.minimizer_progress_to_stdout = true;
    for (size_t i = 0; i < conf.n_node; i++) {
      problem.SetParameterLowerBound(curv1.data(), i, -conf.max_curv);
      problem.SetParameterUpperBound(curv1.data(), i, conf.max_curv);
      problem.SetParameterLowerBound(tors1.data(), i, -conf.max_tors);
      problem.SetParameterUpperBound(tors1.data(), i, conf.max_tors);
    }

    ceres::Solver::Summary summary;
    ceres::Solve(options, &problem, &summary);
    std::cout << summary.BriefReport() << "\n";
    auto app_v = std::vector<Apparatus<double>>(conf.n_node);
    app_v[0] = MakeApparatus<double>(p0, aa0, curv0, tors0);
    for (size_t i = 0; i < conf.n_node - 1; i++) {
      app_v[i + 1] = FrenetTaylor(app_v[i], curv1(i), tors1(i), ds);
    }

    return std::make_tuple(app_v, curv1, tors1);
  }
  // explicxit instanciation
  // template class Apparatus<double>;
  using Myjet = ceres::Jet<double, 48>;
  Myjet absdef;
  template Apparatus<Myjet> FrenetTaylor<Myjet>(const Apparatus<Myjet>& a, const Myjet& kp,
                                                const Myjet& tp, Myjet s);
  // template Apparatus<Myjet> MakeApparatus<Myjet>(const Eigen::Matrix<Myjet, 3, 1>& xyz,
  //                                                const Eigen::Matrix<Myjet, 3, 1>& angle_axis,
  //                                                const Myjet& curv0, const Myjet& tors0);
  template Apparatus<double> FrenetTaylor(const Apparatus<double>& a, const double& kp,
                                          const double& tp, double s);
  // template Apparatus<double> MakeApparatus<double>(const Eigen::Matrix<double, 3, 1>& xyz,
  //                                                  const Eigen::Matrix<double, 3, 1>& angle_axis,
  //                                                  const double& curv0, const double& tors0);
  template std::vector<Apparatus<double>> RailIntegrator(const Apparatus<double>& app0,
                                                         const V1n<double>& curv1,
                                                         const V1n<double>& tors1,
                                                         const double& ds);
  template bool Apparatus<double>::operator==(const Apparatus<double>& a) const;
  template bool RailDistance::operator()(const double* const x0, const double* const AngleAxis,
                                         const double* const curv0, const double* const tors0,
                                         const double* const curv1, const double* const tors1,
                                         const double* const s, double* residual) const;

}  // namespace cloth
