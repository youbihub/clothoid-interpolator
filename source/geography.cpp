#include "rail/geography.h"

#include <iostream>
namespace cloth {
  Eigen::Matrix3Xd Lla2ecef(Eigen::Array3Xd lla) {
    // input parsing
    auto lat = lla.row(0);  // not a copy (?)
    auto lon = lla.row(1);
    auto alt = lla.row(2);

    // https://microem.ru/files/2012/08/GPS.G1-X-00006.pdf Datum Transformations of GPS Positions
    // Application Note 5th July 1999
    auto& phi = lat;
    auto& lambda = lon;
    auto& h = alt;

    auto a = 6378137.;
    auto b = 6356752.3142451;
    auto a2 = a * a;
    auto b2 = b * b;
    auto e = sqrt((a2 - b2) / a2);
    auto e2 = e * e;
    auto s2p = sin(phi) * sin(phi);
    auto N = a / sqrt(1 - e2 * s2p);  // Radius of curvature

    //  results :
    auto xyz = Eigen::Array3Xd(3, lla.cols());

    xyz.row(0) = (N + h) * cos(phi) * cos(lambda);  // x
    xyz.row(1) = (N + h) * cos(phi) * sin(lambda);  // y
    xyz.row(2) = (b2 / a2 * N + h) * sin(phi);      // z

    return xyz;
  }
}  // namespace cloth
