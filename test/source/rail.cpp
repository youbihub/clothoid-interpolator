#include <ceres/ceres.h>
#define DOCTEST_CONFIG_NO_SHORT_MACRO_NAMES
#include <doctest/doctest.h>

#include "rail/rail.h"

DOCTEST_TEST_CASE("myfloor") {
  // only for index. Should not be negative
  DOCTEST_CHECK(cloth::myfloor<double>(5.1234) == 5);
  DOCTEST_CHECK(cloth::myfloor<double>(5.0) == 5);
  DOCTEST_CHECK(cloth::myfloor<double>(0) == 0);
  // Ceres jets
  auto j = ceres::Jet<double, 123>{12.321, 44};
  DOCTEST_CHECK(cloth::myfloor(j) == 12);
}

DOCTEST_TEST_CASE("MakeApparatus") {
  DOCTEST_SUBCASE("zeroapp") {
    auto r = Eigen::Vector3d(1.23, 2.22, -22.21);
    auto a = Eigen::Vector3d(0, 0, 0);
    auto c = 0.;
    auto t = 0.;
    auto y = cloth::MakeApparatus<double>(r, a, c, t);
    auto x = cloth::Apparatus<double>{r, {1, 0, 0}, {0, 1, 0}, {0, 0, 1}, c, t};
    DOCTEST_CHECK(y == x);
  }
  DOCTEST_SUBCASE("looking left") {
    auto r = Eigen::Vector3d(1.23, 2.22, -22.21);
    auto a = Eigen::Vector3d(0, 0, M_PI_2);
    auto c = 0.;
    auto t = 0.;
    auto y = cloth::MakeApparatus<double>(r, a, c, t);
    auto x = cloth::Apparatus<double>{r, {0, 1, 0}, {-1, 0, 0}, {0, 0, 1}, c, t};
    DOCTEST_CHECK(y == y);
  }
  DOCTEST_SUBCASE("tilting head right") {
    auto r = Eigen::Vector3d(1.23, 2.22, -22.21);
    auto a = Eigen::Vector3d(M_PI_2, 0, 0);
    auto c = 0.;
    auto t = 0.;
    auto y = cloth::MakeApparatus<double>(r, a, c, t);
    auto x = cloth::Apparatus<double>{r, {1, 0, 0}, {0, 0, 1}, {0, -1, 0}, c, t};
    DOCTEST_CHECK(y == y);
  }
}

DOCTEST_TEST_CASE("Frenet Taylor") {
  DOCTEST_SUBCASE("s=zero") {
    auto app = cloth::MakeApparatus<double>({1.23, 2.22, -3.11}, {4., 1.5, 2}, 7, 6);
    auto y = cloth::FrenetTaylor<double>(app, 10, 12.5, 0);
    DOCTEST_CHECK(y == app);
  }
  DOCTEST_SUBCASE("straight line") {
    auto r = Eigen::Vector3d{1.23, 2.22, -3.11};
    auto app0 = cloth::MakeApparatus<double>(r, {4., 1.5, 2}, 0, 0);
    auto s = 12.321;
    auto app1 = cloth::FrenetTaylor<double>(app0, 0, 0, s);
    auto z = app0;
    z.R += s * z.T;
    DOCTEST_CHECK(z == app1);
  }
  // DOCTEST_SUBCASE("circle left for small angle") {
  //   auto r = 150.;
  //   auto c = 1. / r;
  //   auto app0 = cloth::MakeApparatus<double>({0, 0, 0}, {0, 0, 0}, c, 0);
  //   auto theta = M_PI_2;
  //   auto s = r * theta;
  //   auto app1 = cloth::FrenetTaylor<double>(app0, 0, 0, s);
  //   auto y = cloth::Apparatus<double>{{r * sin(theta), r * (1. - cos(theta)), 0},
  //                                     {cos(theta), sin(theta), 0},
  //                                     {-sin(theta), cos(theta), 0},
  //                                     {0, 0, 1},
  //                                     c,
  //                                     0};
  //   std::cout << y.R << ',' << app1.R << std::endl;
  //   DOCTEST_CHECK(y == app1);
  // }
}