#include <doctest/doctest.h>
#include <rail/geography.h>

#include <iostream>
#include <string>

TEST_CASE("Lla2ecef") {
  cloth::Lla lla(3, 1);
  auto xyz = Eigen::Matrix3Xd(3, 1);
  // point on equator
  lla << 0, 0, 0;
  double equatorial_radius = 6378137;
  xyz << equatorial_radius, 0, 0;
  CHECK(cloth::Lla2ecef(lla) == xyz);
  // point on north pole
  lla << M_PI_2, 123212321, 0;
  xyz << 0, 0, 6356752.3142;
  CHECK((cloth::Lla2ecef(lla) - xyz).norm() < 0.001);
  CHECK(2.2 == 2.2);
  CHECK(3 == 3);
  // point on equator at 1m high
  lla << 0, 0, 1;
  xyz << equatorial_radius + 1, 0, 0;
  CHECK(cloth::Lla2ecef(lla) == xyz);
  // test from https://tool-online.com/en/coordinate-converter.php
  lla << 42.321, 65.65456, 357.6456;
  lla(Eigen::seq(0, 1), Eigen::all) *= M_PI / 180.;
  xyz << 1947132.045, 4303316.569, 4272274.880;
  CHECK((cloth::Lla2ecef(lla) - xyz).norm() < 0.001);
}