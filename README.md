# Clothoid Interpolator

Function to interpolate points from clothoid curve keypoints. The interpolation is an optimization process, trying to fit the curve as close as possible to the keypoints while trying to minimize the curvature of the curve.