# Get the base Ubuntu image from Docker Hub
FROM ubuntu:latest

# Update apps on the base image
ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update -y && apt-get install -y cmake git g++ libgoogle-glog-dev libatlas-base-dev libeigen3-dev libsuitesparse-dev
COPY . /usr/src/dockertest1

# Specify the working directory
WORKDIR /usr/src/dockertest1

# Use Clang to compile the Test.cpp source file
RUN cmake -Htest -Bbuild/test -DCMAKE_EXPORT_COMPILE_COMMANDS=ON -DUSE_SANITIZER="'Address;Undefined'" -DCMAKE_BUILD_TYPE=Debug
RUN cmake --build build/test -j$(nproc)
# RUN echo ${CPM_SOURCE_CACHE}
RUN ls /home/.cache/CPM

RUN apt-get install python3 python3-pip
RUN pip3 install jinja2 Pygments
RUN cmake -Hdocumentation -Bbuild/doc
RUN cmake --build build/doc --target GenerateDocs -j$(nproc)

# Run the output program from the previous step
CMD build/test/GreeterTests